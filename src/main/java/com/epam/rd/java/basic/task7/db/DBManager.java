package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    public static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)";
    private static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
    public static final String INSERT_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";
    public static final String SELECT_ALL_USERS_FROM_USERS = "SELECT * FROM users";
    public static final String SELECT_ALL_TEAMS_FROM_TEAMS = "SELECT * FROM teams";
    public static final String SELECT_TEAMS_FOR_USER = "SELECT * FROM users_teams WHERE user_id = ?";
    public static final String SELECT_TEAM_BY_ID = "SELECT * FROM teams WHERE id = ?";
    public static final String GET_USER = "SELECT * FROM users WHERE login = ?";
    public static final String GET_TEAM = "SELECT * FROM teams WHERE name = ?";
    public static final String DELETE_USER = "DELETE FROM users WHERE id = ?";
    public static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    public static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";


    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> userList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(getURL());
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL_USERS_FROM_USERS)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = User.createUser(resultSet.getString("login"));
                user.setId(resultSet.getInt("id"));
                userList.add(user);
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new DBException("", sqlException);
        }
        return userList;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection connection = DriverManager.getConnection(getURL());
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER,
                     Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, user.getLogin());
            int changedRows = preparedStatement.executeUpdate();
            if (changedRows == 0) {
                throw new DBException("", new SQLException("No rows was changed"));
            }
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getInt(1));
                } else {
                    throw new DBException("", new SQLException("Occurred problem with id"));
                }
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new DBException("", sqlException);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        for (User user : users) {
            if (deleteElement(DELETE_USER, user.getId())) return true;
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = User.createUser(login);
        getElement(login, user, GET_USER);
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = Team.createTeam(name);
        getElement(name, team, GET_TEAM);
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> allTeams = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(getURL())) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_TEAMS_FROM_TEAMS);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Team team = Team.createTeam(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
                allTeams.add(team);
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new DBException("", sqlException);
        }
        return allTeams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(getURL());
             PreparedStatement statement = connection.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, team.getName());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("No rows affected");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    team.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new DBException("", sqlException);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        try (Connection connection = DriverManager.getConnection(getURL())) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TEAMS_FOR_USER)) {
                for (Team team : teams) {
                    if (team == null) {
                        connection.rollback();
                        throw new DBException("", new NullPointerException());
                    }
                    preparedStatement.setString(1, String.valueOf(user.getId()));
                    preparedStatement.setString(2, String.valueOf(team.getId()));
                    preparedStatement.execute();
                }
            } catch (SQLException sqlException) {
                connection.rollback();
                sqlException.printStackTrace();
                throw new DBException("", sqlException);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new DBException("", sqlException);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teamList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(getURL());
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TEAMS_FOR_USER)) {
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PreparedStatement statement = connection.prepareStatement(SELECT_TEAM_BY_ID);
                statement.setInt(1, resultSet.getInt("team_id"));
                ResultSet rs = statement.executeQuery();
                rs.next();
                Team team = Team.createTeam(rs.getString("name"));
                team.setId(rs.getInt("id"));
                teamList.add(team);
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new DBException("", sqlException);
        }
        return teamList;
    }

    public boolean deleteTeam(Team team) throws DBException {
        if (team == null) return false;
        return deleteElement(DELETE_TEAM, team.getId());
    }

    private boolean deleteElement(String deleteTeam, int id) throws DBException {
        try (Connection connection = DriverManager.getConnection(getURL());
             PreparedStatement preparedStatement = connection.prepareStatement(deleteTeam)) {
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new DBException("", sqlException);
        }
        return true;
    }


    public boolean updateTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(getURL());
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TEAM)) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, team.getId());
            preparedStatement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new DBException("", sqlException);
        }
        return true;
    }

    private String getURL() {
        String URL = null;
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("app.properties"));
            URL = properties.getProperty("connection.url");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return URL;
    }

    private void getElement(String identificationKey, Object obj, String statement) throws DBException {
        try (Connection connection = DriverManager.getConnection(getURL());
             PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
            preparedStatement.setString(1, identificationKey);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                if (obj instanceof User) {
                    ((User) obj).setId(resultSet.getInt("id"));
                } else if (obj instanceof Team) {
                    ((Team) obj).setId(resultSet.getInt("id"));
                }
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new DBException("", sqlException);
        }
    }
}
